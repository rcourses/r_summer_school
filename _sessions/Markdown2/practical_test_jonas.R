

library(tidyverse)
library(kableExtra)
library(glue)

daten <- read_csv("_sessions/_materials/summer_school_data.csv")

daten <- daten %>% 
  mutate(across(starts_with(c("WPK", "EE")),
                ~replace_na(.x, median(.x, na.rm = TRUE)))) %>% 
  mutate(WPK = rowMeans(select(., starts_with("WPK"))),
         EE = rowMeans(select(., starts_with("EE"))))

daten %>% 
  group_by(fkraft) %>% 
  summarise(M_WPK = glue("{round(mean(WPK), 2)} ({round(sd(WPK), 2)})"),
            M_EE = glue("{round(mean(EE), 2)} ({round(sd(EE), 2)})"))

daten %>% 
  group_by(fkraft) %>% 
  summarise(M_WPK = glue("{round(mean(WPK), 2)} ({round(sd(WPK), 2)})"),
            M_EE = glue("{round(mean(EE), 2)} ({round(sd(EE), 2)})")) %>% 
  kbl(align = "lcc")

daten %>% 
  group_by(fkraft) %>% 
  summarise(
    M_WPK = glue("{round(mean(WPK), 2)} ({round(sd(WPK), 2)})"),
    M_EE = glue("{round(mean(EE), 2)} ({round(sd(EE), 2)})")
  ) %>% 
  kbl(align = "lcc",
      col.names = c("Führungskraft",
                    "M<sub>WPK</sub> (SD<sub>WPK</sub>)",
                    "M<sub>EE</sub> (SD<sub>EE</sub>)"),
      escape = FALSE,
      caption = "Tabelle 1: Mittlere Skalenwerte") %>% 
  kable_classic_2(full_width = FALSE)
