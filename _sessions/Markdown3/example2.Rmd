---
title: Markdown
output: html_document
---

```{r setup, include=TRUE, echo=FALSE, message=FALSE, warning=FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.width = 8, fig.height = 8)

library(tidyverse)
library(lubridate)

```



# Überschrift 1

Dieses &lt;span&gt; Wort &lt;/span&gt; soll gestylt werden.


Dieses <span>Wort</span> soll gestylt werden.


<br><br><br>
Dieses &lt;span style="color: red;"&gt; Wort &lt;/span&gt; soll gestylt werden.


Dieses <span style="color: red;">Wort</span> soll gestylt werden.


<br><br><br>


Dieses &lt;span style="color: red; background-color: lightblue;"&gt; Wort &lt;/span&gt; soll gestylt werden.


Dieses <span style="color: red; background-color: lightblue;">Wort</span> soll gestylt werden.











