## NAME
## DATUM
## Wrangling Practical

#Pakete einlesen
library(tidyverse)

daten <- read_csv("_sessions/_materials/soziodemografie.csv")


daten

names(daten)

daten <- daten %>% 
  rename(alter = allter,
         fstatus = X1,
         anzahl_kinder = AK,
         anzahl_mitarbeitende = mao,
         einkommen_euro = EKA)

names(daten)

daten %>% count(anzahl_kinder)

daten %>% count(einkommen_euro)
daten %>% count(anzahl_mitarbeitende)
daten %>% count(fkraft)

daten <- daten %>%
  mutate(anzahl_kinder_num = case_when(
    anzahl_kinder == "1 Kind" ~ 1,
    anzahl_kinder == "2 Kinder" ~ 2,
    anzahl_kinder == "3 Kinder" ~ 3,
    anzahl_kinder == "4 Kinder" ~ 4,
    anzahl_kinder == "keine Kinder" ~ 0))

# Verändere geschlecht
daten <- daten %>%
  mutate(geschlecht = case_when(
    geschlecht == "Weiblich" ~ "w",
    geschlecht == "Männlich" ~ "m"
  )
  )

daten <- daten %>%
  mutate(fkraft_01 = case_when(
    fkraft == "Ja" ~ 1,
    fkraft == "Nein" ~ 0,
  )
  )

daten %>% 
  filter(geschlecht == "m") %>% 
  pull(fkraft_01) %>% 
  mean()

daten %>% 
  filter(geschlecht == "w") %>% 
  pull(fkraft_01) %>% 
  mean()

daten %>% 
  filter(alter < 50) %>% 
  pull(fkraft_01) %>% 
  mean()

daten %>% 
  filter(alter >= 50) %>% 
  pull(fkraft_01) %>% 
  mean()

variablen <- read_csv("_sessions/_materials/variablen.csv")
variablen
names(variablen)

variablen %>% 
  pull(id)


daten_left <- daten %>% 
  left_join(variablen, by = "id")

daten_inner <- daten %>% 
  inner_join(variablen, by = "id")


all_equal(daten_left, daten_inner)


daten <- daten_left

daten_reduziert <- daten %>%
  select(id, geschlecht, alter, anzahl_kinder_num, fkraft_01, AB1, AB2, AB3) %>% 
  filter(alter < 55,
         alter > 25,
         AB1 >= 5,
         AB2 >= 5,
         AB3 >= 5) %>% 
  arrange(fkraft_01,
          desc(alter))%>% 
  slice(1:12)

