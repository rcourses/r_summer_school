getwd()
set.seed(42)
daten <- read_csv("_sessions/_materials/summer_school_data.csv")

soziodem <- daten %>%
  rename(X1 = fstatus, allter = alter) %>%
  select(1:26) %>%
  mutate(id = 1:n(), .before = 1) %>%
  write_csv("_sessions/_materials/soziodemografie.csv")

variablen <- daten %>% 
  select(27:237) %>% 
  mutate(id = 1:n(), .before = 1) %>%
  slice(sample(1:n())) %>% 
  write_csv("_sessions/_materials/variablen.csv")

daten %>% # Engabement Absorbiertheit
  rename(X1 = fstatus, X2 = alter) %>% 
  slice(53:59) %>% 
  mutate(id = 1:7, .before = 1) %>% 
  mutate(X2 = case_when(
    id %in% 1:2 ~ X2 - 13,
    id %in% 3:7 ~ X2)) %>% 
  slice(sample(1:n()))

d2 <- daten %>% 
  select(AB1:AB3) %>% # Engabement Absorbiertheit
  slice(c(53:57, 61:62)) %>% 
  mutate(id = c(1:5, 98:99), .before = 1) %>% 
  slice(sample(1:n()))
