library(tidyverse)

daten <- read.csv("_sessions/DataIO/1_Data/summer_school_data.csv")

daten %>%
  select(alter, geschlecht, fkraft) %>% 
  write.csv("_sessions/DataIO/1_Data/summer_school_data_short.csv",
            row.names = FALSE)
