### R Skript zu "Objekte"
### Kurs "Data Science mit R: erste Schritte"
### Datum: Juni 2024
### Autoren: Markus Steiner und Jonas Mumenthaler


### <- kreiert Objekte ----------------------------

# Was ist Funktion, was ist Objekt
eins_zwei_drei <- c(1, 2, 3)

# Funktioniert das?
c(4, 5, 6) -> vier_fünf_sechs

# Funktioniert das?
sieben_acht_neun = c(7, 8, 9)

# Welchen Assignmnent Operator solltes du verwenden?

# Was ist eins_zwei_drei für ein Objekt?

# Wie viele Elemente hat eins_zwei_drei (Länge)? Berechne mit R-Code

# Was ist das für ein Objekt?
stringstringstringstring <- c("string", "string", "string", "string")

# Wie viele Elemente enthält stringstringstringstring?

# Was ist das hier?
unbekannter_vektor <- c(1, 2, "drei")



### <- verändert Objekte ----------------------------

# Ändert sich hier das Objekt?
eins_zwei_drei + 10 

# Ändert sich hier das Objekt?
eins_zwei_drei_plus10 <- eins_zwei_drei + 10 

# Was kommt hier raus
was_ist_das <- eins_zwei_drei + vier_fünf_sechs 

# Was kommt hier raus
was_ist_das <- eins_zwei_drei + vier_fünf_sechs * sieben_acht_neun 

# Was steckt hinter dem Objekt
was_ist_das 

# Komplett neue Werte für eins_zwei_drei
eins_zwei_drei <- c(4, 5, 6)

# hat es funktioniert?
eins_zwei_drei
