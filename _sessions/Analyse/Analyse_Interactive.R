### R Skript zu "Analyse"
### Kurs "Data Science mit R: Erste Schritte"
### Datum: Juni 2024
### Autoren: Markus Steiner und Jonas Mumenthaler


### Einfache Statistiken ----------------------------

# Lese den Datei summer_school_data.csv als `daten` ein

# Berechne den Mittelwert (mean()) von den Variablen EE5 und TAPW

# Berechne den Median (median()) von den Variablen EE5 und TAPW

# Berechne die Standardabweichung (sd()) von den Variablen EE5 und TAPW

# Berechne die Korrelation (cor()) zwischen den Variablen EE5 und TAPW

### Einfache Grafiken ----------------------------

# Plotte ein Histogram (hist()) für die Variable EE5

# Plotte ein Histogram (hist()) für die Variable TAPW

# Plotte ein Streudiagram (plot()) für die Variablen TAPW und EE5

